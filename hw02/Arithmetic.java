  /*
* Alan Shuo Yang Wang
* CSE 02 HW 02
* 881596402
* */

//Practice manipulating data stored in variables, running simple calculations, printing the numerical output
public class Arithmetic {
    //Main method
    public static void main(String[] args) {
        //Number of pairs of pants
        int numPants = 3;
        //Cost per pair of pants
        double pantsPrice = 34.98;

        //Number of sweatshifts
        int numShirts = 2;
        //Cost per shirt
        double shirtPrice = 24.99;

        //Number of belts
        int numBelts = 1;
        //cost per belt
        double beltCost = 33.99;

        //the tax rate
        double paSalesTax = 0.06;

        //Calculate the total cost of each kind of item
        double totalPant;
        totalPant = numPants * pantsPrice;

        double totalShirt;
        totalShirt = numShirts * shirtPrice;

        double totalBelt;
        totalBelt = numBelts * beltCost;

        //Calculate the total sales tax per item
        double pantTax;
        pantTax = paSalesTax * totalPant;

        double shirtTax;
        shirtTax = paSalesTax * totalShirt;

        double beltTax;
        beltTax = paSalesTax * totalBelt;

        //Calculate the total cost of purchases before tax
        double totalCost;
        totalCost = totalPant + totalShirt + totalBelt;

        //Calculate the total sales tax
        double totalSalesTax;
        totalSalesTax = pantTax + shirtTax + beltTax;

        //Calculate the total price for transaction
        double total;
        total = totalCost + totalSalesTax;

        //Gets 2 decimal places of doubles
        double convertTotal = total * 100; //Multiplies the cost by 100
        int totalInt = (int) convertTotal; //Converts the double to int to get rid of decimals
        double totalDouble = totalInt; //Converts int to double
        totalDouble /= 100; //Divides double by 100 to get 2 decimals

        convertTotal = pantTax * 100;
        totalInt = (int) convertTotal;
        double pantTaxDouble = totalInt;
        pantTaxDouble /= 100;

        convertTotal = shirtTax * 100;
        totalInt = (int) convertTotal;
        double shirtTaxDouble = totalInt;
        shirtTaxDouble /= 100;

        convertTotal = beltTax * 100;
        totalInt = (int) convertTotal;
        double beltTaxDouble = totalInt;
        beltTaxDouble /= 100;

        convertTotal = totalSalesTax * 100;
        totalInt = (int) convertTotal;
        double totalSalesTaxDouble = totalInt;
        totalSalesTaxDouble /= 100;

        convertTotal = total * 100;
        totalInt = (int) convertTotal;
        double subtotalDouble = totalInt;
        subtotalDouble /= 100;

        //Prints the price and sales tax for pants
        System.out.println("Price of pants       : $" + totalPant);
        System.out.println("Sales tax for pants  : $" + pantTaxDouble);
        System.out.println();

        //Prints the price and sales tax for shirts
        System.out.println("Price of shirts      : $" + totalShirt);
        System.out.println("Sales tax for shirts : $" + shirtTaxDouble);
        System.out.println();

        //Prints the price and sales tax for belts
        System.out.println("Price of belts       : $" + totalBelt);
        System.out.println("Sales tax for belts  : $" + beltTaxDouble);
        System.out.println();

        //Prints the price and sales tax for subtotal
        System.out.println("Total cost before tax: $" + totalCost);
        System.out.println("Total sales tax      : $" + totalSalesTaxDouble);
        System.out.println();

        //Prints subtotal and total price of transaction
        System.out.println("Subtotal             : $" + subtotalDouble);
        System.out.println("The total price of transaction: $" + totalDouble);

    }
}