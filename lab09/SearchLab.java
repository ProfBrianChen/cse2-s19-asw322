/*
Alan Shuo Yang Wang
CSE 2 Lab 09
ID: 881596402
*/

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class SearchLab {


	public static int[] fillArrLinear(int length) {
		Random rand = new Random();
		int[] arr = new int[length];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt(length);
		}
		return arr;
	}

	public static int[] fillArrBinary(int length) {
		Random rand = new Random();
		int[] arr = new int[length];
		arr[0] = rand.nextInt(length);
		for(int i = 1; i < arr.length; i++) {
			arr[i] = rand.nextInt((length - arr[i-1]) + 1) + arr[i-1];
		}
		return arr;
	}

	public static int linearSearch(int[] arr, int target) {
		int result = -1;
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == target) {
				return i;
			}
		}
		return result;
	}

	public static int binarySearch(int[] arr, int target) {
		int result = -1;

		int min = 0;
		int max = arr.length - 1; 
		int mid;

		while(min <= max) {
			mid = min + (max - min) / 2;

			if(arr[mid] == target) {
				return mid;
			}

			if(arr[mid] < target) {
				min = mid + 1;
			}
			else {
				max = mid - 1;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);


		int length = 0;
		int target = -1;

		int counter = 0;
        while(counter < 1) { //while loop that continuously runs while
            System.out.print("Enter a positive integer as length: ");
            if(console.hasNextInt()) { //checks if the user input is a number
                length = console.nextInt(); //stores user input into myInt
                if(length > 0) { //checks if myInt is a positive integer
                    counter++; //increments counter to exit while loop
                }
                else { //prints error when the number is a negative
                    System.out.println("Cannot accept negative number");
                }
            }
            else { //prints error when user input is wrong data type
                System.out.println("Wrong Data Type");
                String junkword = console.next(); //stores user input into useless String
            }
        }


        counter = 0;
        while(counter < 1) { //while loop that continuously runs while
            System.out.print("Enter a positive integer as length: ");
            if(console.hasNextInt()) { //checks if the user input is a number
                target = console.nextInt(); //stores user input into myInt
                if(target > 0) { //checks if myInt is a positive integer
                    counter++; //increments counter to exit while loop
                }
                else { //prints error when the number is a negative
                    System.out.println("Cannot accept negative number");
                }
            }
            else { //prints error when user input is wrong data type
                System.out.println("Wrong Data Type");
                String junkword = console.next(); //stores user input into useless String
            }
        }

        counter = 0;
        int action = 0; 
        while(counter < 1) {
        	System.out.print("Enter 1 for linear and 2 for binary: ");
        	if(console.hasNextInt()) {
        		action = console.nextInt();
        		if(action == 1 || action == 2) {
        			counter++;
        		}
        		else {
        			System.out.println("Invalid input");
        		}
        	}
        	else {
        		System.out.println("Wrong Data Type");
        		String junkword = console.next();
        	}
        }

        int[] arr;
        int result;

        if(action == 1) { //linear search
        	arr = fillArrLinear(length);
        	result = linearSearch(arr, target);
        	System.out.println(Arrays.toString(arr) + "\n" + "Target: " + target + " \nResult: " + result);
        }
        else if(action == 2) {
        	arr = fillArrBinary(length);
        	result = binarySearch(arr, target);
        	System.out.println(Arrays.toString(arr) + "\n" + "Target: " + target + " \nResult: " + result);
        }

	}
}