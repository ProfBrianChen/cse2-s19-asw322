/*
* Alan Shuo Yang Wang
* CSE 02 Hw 05
* 881596402
* */

/*
* Write a program that takes in inputs from user and checks if each input is the correct type.
* Return error if incorrect using a while loop
* Assignment does not require any printing
* */

import java.util.Scanner; //import scanner
public class Hw05 {
    public static void main(String[] args) { //main method
        Scanner console = new Scanner(System.in); //scanner object

        int courseNumber = 0; //data field
        String departmentName = "";
        int numberPerWeek = 0;
        String startTime = "";
        String instructorName = "";
        int numberOfStudent = 0;
        int counter;

        counter = 0; //counter to check when input is correct
        while (counter < 1) { //while loop for course number
            System.out.print("Enter course number: ");
            if (console.hasNextInt()) { //checks if the input is an integer
                courseNumber = console.nextInt(); //stores input into courseNumber
                counter++; //increment to break
            } else { //prints error and stores input into unused string
                System.out.println("Input not an integer");
                String junkword = console.next();
            }
        }

        counter = 0; //sets counter to zero
        while (counter < 1) { //while loop for department name
            System.out.print("Enter department name: ");
            if (console.hasNext()) { //checks if the input is a string
                departmentName = console.next(); //stores input into departmentName
                counter++; //increment to break
            } else { //prints error and store input into unused string
                System.out.println("Input not a string");
                String junkword = console.next();
            }
        }

        counter = 0; //sets counter to zero
        while (counter < 1) { //while loop for classes per week
            System.out.print("Enter number of classes per week: ");
            if (console.hasNextInt()) { //checks if input is an integer
                numberPerWeek = console.nextInt(); //stores input into numberPerWeek
                counter++; //increment to break
            } else { //prints error and store input into unused string
                System.out.println("Input not an integer");
                String junkword = console.next();
            }
        }

        counter = 0; //sets counter to zero
        while (counter < 1) { //while loop for start time
            System.out.print("Enter start time as X:XXpm or X.XXam: ");
            if (console.hasNext()) { //checks if input is an integer
                startTime = console.next(); //stores input into startTime
                counter++; //increment to break
            } else { //prints error and store input into unused string
                System.out.println("Input not an integer");
                String junkword = console.next();
            }
        }

        console.nextLine(); //calls an empty console nextLine as placer

        counter = 0; //sets counter to zero
        while (counter < 1) { //while loop for instructor name
            System.out.print("Enter instructor name: ");
            if (console.hasNextLine()) { //checks if input is a string
                instructorName = console.nextLine(); //concats input to instructorName
                counter++; //increment to break
            } else { //prints error and store input into unused string
                System.out.println("Input not a string");
                String junkword = console.nextLine();
            }
        }

        counter = 0; //sets counter to zero
        while (counter < 1) { //while loop for number of students
            System.out.print("Enter number of student: ");
            if (console.hasNextInt()) { //checks if input is an integer
                numberOfStudent = console.nextInt(); //stores input into numberOfStudent
                counter++; //increment to break
            } else { //prints error and store input into unused string
                System.out.println("Input not an integer");
                String junkword = console.next();
            }
        }
    }
}