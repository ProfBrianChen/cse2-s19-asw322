/*
Alan Shuo Yang Wang
CSE 02 Lab 11
881596402
*/

//This program takes in two arrays (best case and worst case) and runs selectionSort on both arrays separately to find the iteration counter
//while printing each iteration of array at the end of each loop
//the method breaks out of the lopo when the array has become sorted 

import java.util.Arrays;
public class Sorting {
	public static void main(String[] args) {
		int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
		int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};

		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);

		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

	//Selection Sort method
	public static int selectionSort(int[] list) {
		System.out.println();
		//Prints the initial array(you must insert another print out statement later in the code to show the array as it's being sorted)
		System.out.println("Initial array: ");
		System.out.println(Arrays.toString(list));

		//Iteration counter
		int iterations = 0;
		int lastIterations = 0;

		//For loop iterating from 0 to last element
		for(int i = 0; i < list.length - 1; i++) {
			
			//Step One: find the minimum in the list[i..list.length-1]
			int currentMin = list[i];
			int currentMinIndex = i;

			for(int j = i+1; j < list.length; j++) {
				if(list[j] < currentMin) {
					currentMin = list[j];
					currentMinIndex = j;
					iterations++;
				}
			}

			//Step Two: swap list[i] with the minimum you found above 
			if(currentMinIndex != i) {
				int temp = list[i];
				list[i] = currentMin;
				list[currentMinIndex] = temp;
			}

			//breaks out of the sort if the array is already sorted
			if(iterations == lastIterations) { 
				break;
			}
			else {
				lastIterations = iterations;
			}
			System.out.println(Arrays.toString(list));
		}
		return iterations;
	}
}