/*
Alan Shuo Yang Wang
CSE 2 Hw 08 
881569402
*/

/*
Create a program that randomly generates an array of characters uppercase and lowercase 
then passes them into 2 methods that returns the a-mA-M values and n-zN-Z values 
then print them
*/
import java.util.Arrays; //import Arrays and Random class
import java.util.Random;

public class Letters {
	public static char[] getAtoM(char[] elem) { //method to return char with values from 'A' to 'M'
		char[] arr1 = new char[elem.length]; //create an empty array with length of elem
		int counter = 0; //index
		for(int i = 0; i < elem.length; i++) { //iterate through the elem array
			if(elem[i] >= 'a' && elem[i] <= 'm') { //conditional for when the char is between 'a' tp 'm'
				arr1[counter] = elem[i]; //stores the value into arr1 at index counter 
				counter++; //increment counter
			}
			if(elem[i] >= 'A' && elem[i] <= 'M') {
				arr1[counter] = elem[i];
				counter++;
			}
		}
		char[] returnArray = new char[counter]; //create another array with size counter 
		for(int i = 0; i < counter; i++) { //iterates from 0 to counter and stores valid values into returnArray
			returnArray[i] = arr1[i];
		}
		return returnArray; //returns array
	}
	public static char[] getNtoZ(char[] elem) { //method to return char with values from 'N' to 'Z'
		char[] arr1 = new char[elem.length]; //uses the same logic as method getAtoM(char[]) except conditional is from 'N' to 'Z'
		int counter = 0;
		for(int i = 0; i < elem.length; i++) {
			if(elem[i] >= 'n' && elem[i] <= 'z') {
				arr1[counter] = elem[i];
				counter++;
			}
			if(elem[i] >= 'N' && elem[i] <= 'Z') {
				arr1[counter] = elem[i];
				counter++;
			}
		}
		char[] returnArray = new char[counter];
		for(int i = 0; i < counter; i++) {
			returnArray[i] = arr1[i];
		}
		return returnArray;
	}

	public static void main(String[] args) { //main method
		Random rand = new Random(); //instantiates random object 
		int size = rand.nextInt((50 - 10) + 1) + 10; //gets a random size from 10 to 50
		// System.out.println("Size = " + size); 
		char[] elem = new char[size]; //creates the char with size length
	
		for(int i = 0; i < elem.length; i++) { //Add random characters to elem array
			int randVal = rand.nextInt((122 - 97) + 1) + 97;
			int temp = rand.nextInt(2); //Gets a random integer from 0 to 1 inclusive 
			if(temp == 0) { //Add random character from a-z
				elem[i] = (char) randVal;
			}
			else if(temp == 1) { //Add random character from A-Z
				elem[i] = (char) (randVal - 32);
			}
		}

		System.out.print("Random character array: "); //prints the random char array
		for(int i = 0; i < elem.length; i++) {
			System.out.print(elem[i]);
		}
		System.out.println();

		char[] arr1 = getAtoM(elem); //calls getAtoM and stores returned char into arr1

		System.out.print("AtoM characters: "); //prints arr1
		for(int i = 0; i < arr1.length; i++) { 
			System.out.print(arr1[i]);
		}
		System.out.println();

		char[] arr2 = getNtoZ(elem); //calls getNtoZ and stores returned char into arr2
		
		System.out.print("NtoZ characters: "); //rpints arr2
		for(int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i]);
		}
		System.out.println();

	}
}