/*
* Alan Shuo Yang Wang
* CSE 2 hw 03
* 881569420
* */

import java.util.Scanner; //imports java scanner
public class Convert {
  public static void main(String[] args) { //main method
    Scanner console = new Scanner(System.in); //instantiate scanner object

    System.out.print("Enter the distance in meters: ");
    double dist = console.nextDouble(); //gets distance in meters and assigns to double
    double distInInches = dist * 100 / 2.54; //converts meters to inches and assigns to double
    System.out.println(dist + " meters is " + (int) (distInInches * 10000) / 10000.0 + " inches."); //prints the inches with 4 decimals
  }
}