/*
* Alan Shuo Yang Wang
* CSE 2 Hw 03
* 881596402
* */
import java.util.Scanner; //Imports java scanner
public class BoxVolume {
    public static void main(String[] args) { //Main method
        Scanner console = new Scanner(System.in); //Instantiate scanner object
        System.out.print("The width side of the box: ");
        double width = console.nextInt(); //gets user width and assign to double
        System.out.print("The length of the box is: ");
        double length  = console.nextInt(); //gets user length and assign to double
        System.out.print("The height of the box is: ");
        double height = console.nextInt(); //gets user height and assign to double

        System.out.println("The volume inside the box is: " + (int) (width * length * height)); //returns volume in terms of integer
    }
}