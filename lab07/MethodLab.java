import java.util.Scanner;
import java.util.Random;

public class MethodLab {
    public static Random rand = new Random();
    public static String getAdjective() {
        int num = rand.nextInt(10);
        String myAdjective = "";

        switch(num) {
            case 0:
                myAdjective = "amazing";
                break;
            case 1:
                myAdjective = "brown";
                break;
            case 2:
                myAdjective = "small";
                break;
            case 3:
                myAdjective = "huge";
                break;
            case 4:
                myAdjective = "beautiful";
                break;
            case 5:
                myAdjective = "smart";
                break;
            case 6:
                myAdjective = "dull";
                break;
            case 7:
                myAdjective = "powerful";
                break;
            case 8:
                myAdjective = "energetic";
                break;
            case 9:
                myAdjective = "lazy";
                break;
            default:
                myAdjective = "";
        }
        return myAdjective;
    }

    public static String getSubject() {
        int num = rand.nextInt(10);
        String mySubject = "";

        switch(num) {
            case 0:
                mySubject = "fox";
                break;
            case 1:
                mySubject = "elephant";
                break;
            case 2:
                mySubject = "dog";
                break;
            case 3:
                mySubject = "cat";
                break;
            case 4:
                mySubject = "ladybug";
                break;
            case 5:
                mySubject = "gorilla";
                break;
            case 6:
                mySubject = "chimpanzee";
                break;
            case 7:
                mySubject = "bull";
                break;
            case 8:
                mySubject = "cow";
                break;
            case 9:
                mySubject = "sheep";
                break;
            default:
                mySubject = "";
        }
        return mySubject;
    }

    public static String getVerb() {
        int num = rand.nextInt(10);
        String myVerb = "";

        switch(num) {
            case 0:
                myVerb = "passed";
                break;
            case 1:
                myVerb = "admired";
                break;
            case 2:
                myVerb= "attacked";
                break;
            case 3:
                myVerb = "applauled";
                break;
            case 4:
                myVerb = "appreciated";
                break;
            case 5:
                myVerb = "asked";
                break;
            case 6:
                myVerb = "caught";
                break;
            case 7:
                myVerb = "awoke";
                break;
            case 8:
                myVerb = "scared";
                break;
            case 9:
                myVerb = "freightened";
                break;
            default:
                myVerb = "";
        }
        return myVerb;
    }

    public static String getNoun() {
        int num = rand.nextInt(10);
        String myNoun = "";

        switch(num) {
            case 0:
                myNoun = "plant matter";
                break;
            case 1:
                myNoun = "afterburners";
                break;
            case 2:
                myNoun= "flowers";
                break;
            case 3:
                myNoun = "a hose";
                break;
            case 4:
                myNoun = "a helicopter";
                break;
            case 5:
                myNoun = "shoes";
                break;
            case 6:
                myNoun = "a car";
                break;
            case 7:
                myNoun = "a computer";
                break;
            case 8:
                myNoun = "the tree";
                break;
            case 9:
                myNoun = "the rock";
            default:
                myNoun = "";
        }
        return myNoun;
    }



    public static void createParagraph() {
        Scanner console = new Scanner(System.in);
        int action = 1;

        String mySubject = "";
        String myVerb = "";
        String myObject = "";

        while (true) {
            mySubject = getSubject();
            myObject = getSubject();
            myVerb = getVerb();

            System.out.println();
            System.out.println("The " + getAdjective() + " " + getAdjective() + " " + mySubject + " " + myVerb + " the " + getAdjective() + " " + myObject);

            //<It> used <afterburners> to <spew> <plant matter> at the <fearsome> <cat>.
            System.out.println("It used " + getNoun() + " to " + getVerb() + " " + getNoun() + " at the " + getAdjective() + " " + myObject);
            //That <fox> <knew> her <bulldozers>!
            System.out.println("That " + mySubject + " " + getVerb() + " her " + getNoun());
            System.out.println();

            System.out.println("Enter 1 to continue. Enter any other number to exit: ");
            action = console.nextInt();

            if (action != 1) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        createParagraph();
    }
}