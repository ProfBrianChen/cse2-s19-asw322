/*
Alan Shuo Yang Wang
CSE 2 Hw07 
881596402
*/

import java.util.Scanner; //import scanner 
public class Area {
    public static String getInput() { //method to get inputs from user for shape type
        Scanner console = new Scanner(System.in); //scanner object
        String shape = "";
        while(true) { //infinite while loop until correct data type
            System.out.println("Enter a shape (rectangle or triangle or circle): ");
            shape = console.next(); //takes an input

            if(shape.equals("rectangle") || shape.equals("triangle") || shape.equals("circle")) { //checks if the input is one of three types 
                break;
            }
            else {
                System.out.println("Invalid Input"); //returns error messege if incorrect string
            }
        }
        return shape;
    }

    public static double getDouble(String s) { //method to get double from user 
        Scanner console = new Scanner(System.in); 
        int counter = 0; 
        double len = 0;

        while (counter < 1) { //while loop for len
            System.out.print(s);
            if (console.hasNextDouble()) { //checks if the input is an double
                len = console.nextDouble(); //stores input into len
                if(len > 0) {
                    counter++; //increment to break
                }
                else {
                    System.out.println("Cannot accept negative number");
                }
            } 
            else { //prints error and stores input into unused string
                System.out.println("Input not a double");
                String junkword = console.next();
            }
        }
        return len;
    }

    public static void getRectDimension() { //method to get and print area of rectangle
        Scanner console = new Scanner(System.in);
        double length = 0;
        double width = 0;
        int counter;

        length = getDouble("Enter length: ");
        width = getDouble("Enter width: ");

        double area = length * width;
        System.out.println("The area of the rectangle is: " + area);
    }

    public static void getTriDimension() { //method to get and print area of triangle
        Scanner console = new Scanner(System.in);
        double length = 0;
        double height = 0;
        int counter;

        length = getDouble("Enter base length: ");
        height = getDouble("Enter height: ");

        double area = length * height / 2;
        System.out.println("The area of the triangle is: " + area);
    }

    public static void getCirDimension() {  //method to get and print area of circle
        Scanner console = new Scanner(System.in);
        double radius = 0;
        int counter;

        radius = getDouble("Enter radius: ");

        double area = Math.PI * radius * radius;
        System.out.println("The area of the circle is: " + area);
    }

    public static void main(String[] args) {
        String shape = getInput();
        if(shape.equals("rectangle")) {
            getRectDimension();
        }
        else if(shape.equals("triangle")) {
            getTriDimension();
        }
        else if(shape.equals("circle")) {
            getCirDimension();
        }
    }
}