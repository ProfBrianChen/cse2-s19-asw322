/*
Alan Shuo Yang Wang
CSE 2 Hw07
881596402
*/
import java.util.Scanner; //import scanner 
public class StringAnalysis {
	public static boolean checkString(String s) { //checks if the whole string is valid
		boolean val = false; 
		int counter = 0; 
		for(int i = 0; i < s.length(); i++) { //for loop from 0 to length - 1
			if(Character.isLetter(s.charAt(i))) { //checks if the character at each index is valid 
				counter++; //increment counter
			}
		}
		if(counter == s.length()) { //if counter == length then the whole string is valid
			val = true; 
		}
		return val; //return boolean
	}
	public static boolean checkString(String s, int range) { //checks if part of the string is valid
		boolean val = false;
		int counter = 0; 
		if(range > s.length()) { //passes s to checkString(String s) if the range > string length
			val = checkString(s);
		}
		else {
			for(int i = 0; i < range; i++) { //for loop from 0 to range
				if(Character.isLetter(s.charAt(i))) { //checks if the character at each index is valid
					counter++; //increment counter
				}
			}
			if(counter == range) { //if counter == range then part we checked is valid 
				val = true;
			}
		}
		return val; //return boolean
	}

	public static void init() { //starter method
		boolean result = false;
		Scanner console = new Scanner(System.in); //scanner object
		System.out.println("Enter a string to check: ");
		String s = console.nextLine(); //gets any line from user (do not need to check)
		int action = 0;
		int counter = 0; 

        while (counter < 1) { //while loop to get action 
            System.out.println("Enter 1 to check whole string.\nEnter 2 to check a portion: ");
            if (console.hasNextInt()) { //checks if the input is an integer
                action = console.nextInt(); //stores input into action
                if(action == 1 || action == 2) {
                    counter++; //increment to break
                }
                else {
                    System.out.println("Invalid action");
                }
            } 
            else { //prints error and stores input into unused string
                System.out.println("Input not a integer");
                String junkword = console.next();
            }
        }

		if(action == 1) { //calls checkString(String s) if user wants to check whole string
			result = checkString(s);
		}
		else if(action == 2) { //calls checkString(String s, int range) if user wants to check part of string
			int range = 0;
			counter = 0; 

	        while (counter < 1) { //while loop for range
	            System.out.println("Enter an integer as range: ");
	            if (console.hasNextInt()) { //checks if the input is an integer
	                range = console.nextInt(); //stores input into range
	                if(range >= 0) {
	                    counter++; //increment to break
	                }
	                else {
	                    System.out.println("Cannot be negative");
	                }
	            } 
	            else { //prints error and stores input into unused string
	                System.out.println("Input not a integer");
	                String junkword = console.next();
	            }
	        }
			result = checkString(s, range);
		}

		if(result == true) { //printing
			System.out.println("The string only contains characters");
		}
		else {
			System.out.println("The string does not only contain characters");
		}
	}

	public static void main(String[] args) {
		init(); //calls init method
	}
}