/*
Alan Shuo Yang Wang
CSE 02 Lab 08
881596402
*/
import java.util.Random; //import Random and Arrays class
import java.util.Arrays;

public class ArrayLab {
	public static int getRange(int[] elem) { //method getRange to get the range of the array 
		Arrays.sort(elem);
		return elem[elem.length -1] - elem[0];
	}
	public static double getMean(int[] elem) { //method getMean to get the mean of the array
		double total = 0;
		for(int i = 0; i < elem.length; i++) {
			total += elem[i];
		}
		return total / elem.length;
	}
	public static double getStdev(int[] elem) { //method getStdevv to get the standard deviation of the array
		double mean = getMean(elem);
		double a = 0;
		for(int i = 0; i < elem.length; i++) {
			a += Math.pow((elem[i] - mean), 2);
		}
		a /= (elem.length - 1);
		a = Math.sqrt(a);
		return a;
	}
	public static void shuffle(int[] elem) { //method shuffle to reshuffle the deck with random indexes
		Random rand = new Random();
		for(int i = 0; i < elem.length; i++) {
			int indexA = rand.nextInt(elem.length-1);
			int indexB = rand.nextInt(elem.length-1);
			int temp = elem[indexA];
			elem[indexA] = elem[indexB];
			elem[indexB] = temp;
			}
	}		
	public static void main(String[] args) { //main method 
		Random rand = new Random(); //instantiates a random object 
		int size = rand.nextInt((100 - 50) + 1) + 50; //gets a random size
		System.out.println("Size = " + size);
		int[] elem = new int[size]; //assigns an empty array to the length of size
		for(int i = 0; i < elem.length; i++) { //gets random integers from 0 - 99 for all indexes of elem
			elem[i] = rand.nextInt(99);
			System.out.println(elem[i]);
		}
		int range = getRange(elem); //gets range
		System.out.println("Range = " + range);
		double mean = getMean(elem); //gets mean
		System.out.println("Mean = " + mean);
		double stdev = getStdev(elem); //gets stdev
		System.out.println("Standard Deviation = " + stdev);
		shuffle(elem);
		for(int i = 0; i < elem.length; i++) { //reshuffles the array and prints
			System.out.println(elem[i]);
		}
	}
}