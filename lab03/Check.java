/*
CSE 02 lab 03
Alan Shou Yang Wang
881596402
*/


//Import scanner from java.util.Scanner;
import java.util.Scanner;

public class Check {
  public static void main(String[] args) {
    //declare an instance of Scanner object from java.util.Scanner
    Scanner console = new Scanner(System.in);

    //Displays basic instructions to user to enter cost
    System.out.print("Enter the original cost of the check in the form xx.xx: ");

    //Scanner looking for a userInput
    double checkCost = console.nextDouble();

    //Displays basic instructions to user to enter tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");

    //Scanner looking for percentage
    double tipPercent = console.nextDouble();

    //Convert from percent to decimal
    tipPercent /= 100;

    //Display instructions
    System.out.print("Enter the number of people who went out to dinner: ");

    //Scanner waiting for integer input
    int numPeople = console.nextInt();

    //Declares variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;

    //Gets the total cost by multiplying check by 1 + tip percent
    totalCost = checkCost * (1 + tipPercent);

    //Splits totalCost to each person
    costPerPerson = totalCost / numPeople;

    //truncates costPerPerson into dollars integer
    dollars=(int)costPerPerson;

    //Gets the tenth place in double by multiplying double by 10 then using mod to get integer
    dimes=(int)(costPerPerson * 10) % 10;

    //Gets the hundredth place in double by multiplying double by 100 the nusing mod to get integer
    pennies=(int)(costPerPerson * 100) % 10;

    //Prints total cost per person using String concatentation
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

  }
}
