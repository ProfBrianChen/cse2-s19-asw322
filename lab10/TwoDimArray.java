/*
Alan Shuo Yang Wang
CSE 02 Lab 10
881596402
*/

/*
This program uses increasingMatrix() printMatrix() translate() and addMatrix() 
method to perform changes or copies to 2 dimensional arrays that have a random 
width and height
*/

import java.util.Arrays;
import java.util.Random;

public class TwoDimArray {
	public static int[][] increasingMatrix(int width, int height, boolean format) {
		//format true = row-major 
		int counter = 1;
		int[][] elem = new int[height][width];
		if(format == false) { //do column major
			for(int i = 0; i < elem.length; i++) {
				for(int j = 0; j < elem[i].length; j++) {
					elem[i][j] = counter;
					counter++;
				}
			}
			counter = 0;
		}
		else {	 //do row major
			elem = new int[width][height];
			for(int i = 0; i < height; i++) {
				for(int j = 0; j < width; j++) {
					elem[j][i] = counter;
					counter++;
				}
			}
		}
		return elem;
		
	}
	public static void printMatrix(int[][] elem, boolean format) {
		//format true = row-major
		for(int i = 0; i < elem.length; i++) {
			for(int j = 0; j < elem[i].length; j++) {
				System.out.print(elem[i][j] + "\t");
			}
			System.out.println("\n");
		}
	}
	public static int[][] translate(int[][] elem) {
		//assume column major format -> row major format
		int width = elem.length;
		int height = elem[0].length;
		int[][] newElem = new int[height][width];
		for(int i = 0; i < newElem.length; i++) {
			for(int j = 0; j < newElem[i].length; j++) {
				newElem[i][j] = elem[j][i];
			}
		}
		return newElem;
	}
	public static int[][] addMatrix(int[][] elema, boolean formata, int[][] elemb, boolean formatb) {
		//format true = row major 
		
		if(formata == false) {
			elema = translate(elema);
		}
		else if(formatb == false) {
			elemb = translate(elemb);
		}
		if(elema.length != elemb.length || elema[0].length != elemb[0].length) {
			return null;
		}
		else {
			int[][] result = new int[elema.length][elema[0].length];

			for(int i = 0; i < result.length; i++) {
				for(int j = 0; j < result[i].length; j++) {
					result[i][j] = elema[i][j] + elemb[i][j];
				}
			}
			return result;
		}
	}
	public static void main(String[] args) {
		Random rand = new Random();
		int width1 = rand.nextInt(20 - 10) + 10;
		int height1 = rand.nextInt(20 - 10) + 10;
		int width2 = rand.nextInt(20 - 10) + 10;
		int height2 = rand.nextInt(20 - 10) + 10;


		int[][] elem1 = increasingMatrix(width1, height1, true);
		int[][] elem2 = increasingMatrix(width1, height1, false);
		int[][] elem3 = increasingMatrix(width2, height2, false);

		System.out.println("elem1");
		printMatrix(elem1, true);
		System.out.println("elem2");
		printMatrix(elem2, false);
		System.out.println("elem3");
		printMatrix(elem3, false);

		int[][] elem12 = addMatrix(elem1, true, elem2, false);	
		// for(int i = 0; i < elem12.length; i++) {
		// 		System.out.println(Arrays.toString(elem12[i]));
		// }
		int[][] elem13 = addMatrix(elem1, true, elem3, false);
		// for(int i = 0; i < elem13.length; i++) {
		// 		System.out.println(Arrays.toString(elem13[i]));
		// }

		System.out.println("elem1 + elem2");
		printMatrix(elem12, true);
		System.out.println("elem1 + elem3"); //Will print a nullpointerexception error because dimension not same
		printMatrix(elem13, true);
	}
}