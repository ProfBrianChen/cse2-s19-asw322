import java.util.Scanner;// import scanner class

public class Network {
    public static void main(String[] args) { //main method
        Scanner console = new Scanner(System.in); //scanner object

        int h = 0; //data fields
        int w = 0;
        int s = 0;
        int l = 0;

        int index = 0;

        //get height
        while (index < 1) {
            System.out.print("Input your desired height: ");
            if (console.hasNextInt()) {
                h = console.nextInt();
                if (h > 0) { //if h is positive
                    index++; //increment to break
                } else {
                    System.out.println("Cannot accept negative number");
                }
            } else {
                System.out.println("Error: please type in an integer.");
                String junkword = console.next();
            }
        }
        index = 0; //resets counter

        //get width
        while (index < 1) {
            System.out.print("Input your desired width: ");
            if (console.hasNextInt()) {
                w = console.nextInt();
                if (w > 0) {
                    index++;
                } else {
                    System.out.println("Cannot accept negative number");
                }
            } else {
                System.out.println("Error: please type in an integer.");
                String junkword = console.next();
            }
        }
        index = 0;

        //get square size
        while (index < 1) {
            System.out.print("Input square size: ");
            if (console.hasNextInt()) {
                s = console.nextInt();
                if (s > 0) {
                    index++;
                } else {
                    System.out.println("Cannot accept negative number");
                }
            } else {
                System.out.println("Error: please type in an integer.");
                String junkword = console.next();
            }
        }
        index = 0;

        //get edge length
        while (index < 1) {
            System.out.print("Input edge length: ");
            if (console.hasNextInt()) {
                l = console.nextInt();
                if (l > 0) {
                    index++;
                } else {
                    System.out.println("Cannot accept negative number");
                }
            } else {
                System.out.println("Error: please type in an integer.");
                String junkword = console.next();
            }
        }

        //If the window is smaller than single square
        if(s > w || s > h) { //Completed
            int verticalCounter = 0;
            int horizontalCounter = 0;
            for(int i = 1; i <= h; i++) {
                horizontalCounter = 0;
                if(verticalCounter < h) {
                    if(i == 1) { //If it is the first or last row
                            System.out.print("#"); //Prints out the first pound
                            horizontalCounter++;
                            for(int j = 1; j <= w - 1; j++) { //Prints out the bars
                                System.out.print("-");
                                horizontalCounter++;
                            }

                            if(horizontalCounter < w -1) {
                                System.out.print("#"); //Prints out the last pound
                            }
                    }
                    else {
                            System.out.print("|");
                            horizontalCounter++;
                            for(int j = 1; j <= w - 2; j++) {
                                System.out.print(" ");
                                horizontalCounter++;
                            }

                            if(horizontalCounter < w - 1) {
                                System.out.print("|");
                            }
                    }
                }
                verticalCounter++;
                System.out.println(); //Prints new line
            }
        }

        //If the size is even
        else if(s % 2 == 0) {
            int verticalCounter = 0;
            int horizontalCounter = 0;

            while(verticalCounter < h) { //while loop iterating vertically
                horizontalCounter = 0; //reset horizontalCounter to zero

                if(verticalCounter % (s + l) >= 0 && verticalCounter % (s + l) <= s - 1) { //If the vertical counter is inside the block
                    while(horizontalCounter < w) {

                        if (verticalCounter % (s + l) == 0 || verticalCounter % (s + l) == s - 1) { //prints bars and dashes

                            if (horizontalCounter % (s + l) == 0 || horizontalCounter % (s + l) == s -1) {
                                System.out.print("#");
                                horizontalCounter++;
                            } else if (horizontalCounter % (s + l) > 0 && horizontalCounter % (s + l) < s - 1) {
                                System.out.print("-");
                                horizontalCounter++;
                            } else if (horizontalCounter % (s + l) > s - 1 && horizontalCounter % (s + l) < s + l) {
                                System.out.print(" ");
                                horizontalCounter++;
                            }

                        }
                        else if (verticalCounter % (s + l) > 0 && verticalCounter % (s + l) < s - 1) { //prints lines and spaces

                            if (horizontalCounter % (s + l) == 0 || horizontalCounter % (s + l) == s - 1) {
                                System.out.print("|");
                                horizontalCounter++;
                            } else if (horizontalCounter % (s + l) > 0 && horizontalCounter % (s + l) < s - 1) {
                                System.out.print(" ");
                                horizontalCounter++;
                            } else if (horizontalCounter % (s + l) > s - 1 && horizontalCounter % (s + l) < s + l) {
                                if (verticalCounter % (s + l) == s / 2 || verticalCounter % (s + l) == s / 2 - 1) {
                                    System.out.print("-");
                                    horizontalCounter++;
                                } else {
                                    System.out.print(" ");
                                    horizontalCounter++;
                                }
                            }
                        }
                    }
                }

                else if(verticalCounter % (s + l) > s - 1 && verticalCounter % (s + l) < s + l) {
                    while(horizontalCounter < w) {
                        if(horizontalCounter % (s + l) == s / 2 || horizontalCounter % (s + l) == s / 2 - 1) {
                            System.out.print("|");
                            horizontalCounter++;
                        }
                        else {
                            System.out.print(" ");
                            horizontalCounter++;
                        }
                    }
                }
                verticalCounter++; //iterate verticalCounter after each line
                System.out.println();
            }
        }

        //If the size is odd
        else if(s % 2 == 1) {
            int verticalCounter = 0;
            int horizontalCounter = 0;

            while(verticalCounter < h) { //while loop iterating vertically
                horizontalCounter = 0; //reset horizontalCounter to zero

                if(verticalCounter % (s) >= 0 && verticalCounter % (s) <= s - 1) { //If the vertical counter is inside the block
                    while(horizontalCounter < w) {

                        if (verticalCounter % (s) == 0) { //prints bars and dashes

                            if (horizontalCounter % (s) == 0) {
                                System.out.print("#");
                                horizontalCounter++;
                            }
                            else if (horizontalCounter % (s) > 0 && horizontalCounter % (s) <= s - 1) {
                                System.out.print("-");
                                horizontalCounter++;
                            }
                        }

                        else if (verticalCounter % (s) > 0 && verticalCounter % (s) <= s - 1) { //prints lines and spaces

                            if (horizontalCounter % (s) == 0) {
                                System.out.print("|");
                                horizontalCounter++;
                            } else if (horizontalCounter % (s) > 0 && horizontalCounter % (s) <= s - 1) {
                                System.out.print(" ");
                                horizontalCounter++;
                            }
                        }
                    }
                }
                verticalCounter++; //iterate verticalCounter after each line
                System.out.println();
            }
        }
    }
}