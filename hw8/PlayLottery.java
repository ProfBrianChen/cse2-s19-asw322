/*
Alan Shuo Yang Wang
CSE 2 Hw 08 
881596402
*/

/*
Create a program that randomly generates 5 numbers as lottery numbers then 
asks the user to enter 5 integers (check if input is valid) then checks if the 
value is the same as winning array
finally, print results to user 
*/
import java.util.Scanner; //imports Scanner Random and Array class 
import java.util.Random;
import java.util.Arrays;

public class PlayLottery {
	public static boolean userWins(int[] user, int[] winning) { //userWins method to compare the values and positions of user and winning array
		int counter = 0;
	//returns true when user and winning are the same.
		for(int i = 0; i < user.length; i++) {
			if(user[i] == winning[i]) {
				counter++;
			}
		}

		if(counter == user.length) { //returns true if user wins
			return true;
		}
		return false; //else returns false
	}	

	public static int[] numbersPicked() { //numbersPicked method to return a integer array with 5 random values from 0 to 59
	//generates the random numbers for the lottery without 
	//duplication.
		Random rand = new Random();
		int[] winning = new int[5];

		boolean bool = false; //boolean to check duplicates 
		int i = 0; //index
		while(i < winning.length) {
			int val = rand.nextInt(60); //gets random value from Random
			for(int j = 0; j < i; j++) { //iterates from 0 to ith index to check if value is a duplicate
				if(winning[j] == val) { //set bool to true if it is a duplicate 
					bool = true; 
				}
			}
			if(bool == false) { 
				winning[i] = rand.nextInt(60);
				i++;
			}
		}
		return winning;
	}	

	public static void main(String[] args) { //main method 
		Scanner console = new Scanner(System.in); //scanner object
		
		int[] user = new int[5]; //instantiates empty array of length 5 and stores to user
		int[] winning = numbersPicked(); //gets int[] array from numbersPicked and stores to winning
		
		System.out.print("Enter 5 numbers between 0 and 59: "); //prompt 
		int counter = 0; 
		while(counter < 5) { //while loop to get the 5 inputs 
			int index = 0;
			while(index < 1) { 
				if(console.hasNextInt()) { //checks if userInput is an integer
					int userInput = console.nextInt();
					if(userInput >= 0 && userInput <= 59) { //checks if userInput is in range
						user[counter] = userInput; //stores input 
						index++; //increment index and counter 
						counter++;
					}
					else { //prints error otherwise 
						System.out.print("Invalid range. Enter again: "); 
					}
				}
				else { //prints error otherwise
					System.out.print("Incorrect data type. Enter again: ");
					String junkword = console.next();
				}
			}
		}
		boolean val  = userWins(user, winning); //boolean to see if user won or lost 
		if(val == true) { 
			System.out.println("You win");
		}
		else {
			System.out.println("You lose");
			System.out.print("The winning numbers are: "); //prints values when user loses
			for(int i = 0; i < winning.length - 1; i++) {
				System.out.print(winning[i] + ", ");
			}
			System.out.println(winning[winning.length - 1]);
		}
	}
}