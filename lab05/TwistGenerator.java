/*
* Alan Shuo Yang Wang
* CSE 2 Lab 05
* 881596402
* */

//TwistGenerator takes a positive user input integer then prints out a pattern of 3 lines that has the same length as the userInput
//This is done through different while loops that looks at the top, middle, and bottom row of the print block
//Using a counter/index, we increment by 3 to print each section of 1. "\/ " 2 " X " 3. "/\ "

import java.util.Scanner; //Import Scanner
public class TwistGenerator {
    public static void main(String[] args) { //main method
        Scanner console = new Scanner(System.in); //instantiate scanner object
        int length = 0; //creates data fields
        int counter = 0;
        int i;
        while(counter < 1) { //while loop that continuously runs while
            System.out.print("Enter a positive integer as length: ");
            if(console.hasNextInt()) { //checks if the user input is a number
                length = console.nextInt(); //stores user input into myInt
                if(length > 0) { //checks if myInt is a positive integer
                    counter++; //increments counter to exit while loop
                }
                else { //prints error when the number is a negative
                    System.out.println("Cannot accept negative number");
                }
            }
            else { //prints error when user input is wrong data type
                System.out.println("Wrong Data Type");
                String junkword = console.next(); //stores user input into useless String
            }
        }

        //Printing block

        //Print top row
        System.out.print("\\ ");

        i = 0; //sets index to 0
        while(i < length -2) { //increment from 0 to myInt - 2 because of first print
            System.out.print("/\\ "); //prints "/\ "
            i = i + 3; //increment index by 3
        }
        System.out.println(); //prints space

        //print middle row
        i = 0;
        while(i < length - 2) {
            System.out.print(" X "); //prints " X "
            i = i + 3;
        }
        System.out.println(); //prints space


        //prints bottom row
        System.out.print("/ ");

        i = 0;
        while(i < length -2) {
            System.out.print("\\/ "); //prints "\/ "
            i = i + 3;
        }
        System.out.println(); //prints space

    }
}