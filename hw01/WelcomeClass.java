/*
CSE 02 Hw 01
Alan Shuo Yang Wang
881596402
*/

//Declare the class WelcomeClass
public class WelcomeClass {
  //Create main method
  public static void main(String[] args) {

    //Prints the first three lines
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");

    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");

    //Prints ASW322
    System.out.println("<-A--S--W--3--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    //My autobiographical tweet
    System.out.println("\nMy name is Alan Wang. I am a freshman at Lehigh University. I just submitted my petition form to transfer into the intercollegiate college for CSB on 1/25/2019.");
  }
}
