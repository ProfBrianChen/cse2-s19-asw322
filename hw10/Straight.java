/*
Alan Shou Yang Wang
CSE 2 Hw 10
881596402	
*/

/*
This program uses the implementation of several methods
shuffle(), drawCard(int[]), contains(int[], int), searchCard(int[], int), and straight(int[]) 
to create a random array of 52 cards then draw a random hand from the array and check if the 
hand is a straight. Finally, print out the percentage of straights for 1 million runs
*/

//My program returns an average chance of drawing a straight as roughly 0.42 % which is slightly higher 
//than the wikipedia source of 0.3925 %

import java.util.Arrays;
import java.util.Random;

public class Straight {

	//Generate a shuffled deck of cards of int[] holding 52 cards
	public static int[] shuffle() {
		Random rand = new Random();
		int[] deck = new int[52]; //Array indicies 0 - 51 are accessible
		for(int i = 0; i < deck.length; i++) { //adds values of 0 to 51 to deck
			deck[i] = i;
		}
		for(int i = 0; i < deck.length; i++) { //iterates through the deck to switch values
			int a = rand.nextInt(52);
			int b = rand.nextInt(52);

			int temp = deck[a];
			deck[a] = deck[b];
			deck[b] = temp;
		}
		return deck;
	}

	//Takes in a deck with random numbers and returns the first five numbers in an int[]
	public static int[] drawCard(int[] deck) {
		int[] hand = new int[5];
		for(int i = 0; i < 5; i++) {
			hand[i] = deck[i] % 13;
		}
		return hand; 
	}

    //Finds the value of card in hand at index of k 
	public static int searchCard(int[] hand, int k) {
		if(k > 5 || k < 0) {
			return -1;
		}
		else {
			Arrays.sort(hand);
			return hand[k];
		}
	}

	//Finds the status of the straight with the given hand
	public static boolean straight(int[] hand) {
		//Declaration
		int[] straight = new int[5];

		//Assignment
		for(int i = 0; i < straight.length; i++) {
			straight[i] = searchCard(hand, i);
		}

		// System.out.println(Arrays.toString(straight));

		//Data field
		int status1 = 0;

		//Search for truth
		for(int i = 1; i < straight.length; i++) {
			if(straight[i] == straight[i-1] + 1) {
				status1++;
			}
			else {
				status1 = 0;
				return false;
			}
		}
		//Check truths and return 
		if(status1 == 4) {
			return true;
		}
		return false;
	} 

	//Main method
	public static void main(String[] args) {
		int counter = 0; 
		System.out.println("Doing one million tests..");
		//For loop to iterate 1 million times
		for(int i = 0; i < 1000000; i++) {
			int[] deck = shuffle();
			int[] hand = drawCard(deck);
			//System.out.println(Arrays.toString(hand));
			boolean status = straight(hand);
			if(status == true) {
				counter++;
			}
		}

		//Prints the results
		double straightPercent = counter / 1000000.0;
		System.out.println("Chance = "  + (straightPercent * 100) + "%");
	}
}
