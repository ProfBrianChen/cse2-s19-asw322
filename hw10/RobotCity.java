/*
Alan Shuo Yang Wang
CSE 2 Hw 10
881596402
*/

/*
RobotCity program uses 4 methods
buildCity(), display(), invade(), and update() 
to create a random dimension 2d array of random integers and 
alters a random k integer of elements into negative numbers 
then updates the map 5 times by moving the robots to the right by 1 
*/

//Imports Arrays to Random 
import java.util.Arrays; 
import java.util.Random;
public class RobotCity {

	/*
	Build city creates a random 2d array of dimensions 10-15
	and fills it with random numbers from 100 - 999
	*/
	public static int[][] buildCity() { 
		Random rand = new Random();
		int width = rand.nextInt((15 - 10) + 1) + 10;
		int height = rand.nextInt((15 - 10) + 1) + 10;

		int[][] cityArray = new int[height][width];
		for(int i = 0; i < cityArray.length; i++) {
			for(int j = 0; j < cityArray[i].length; j++) {
				cityArray[i][j] = rand.nextInt((999-100) + 1) + 100;
			}
		}
		return cityArray;
	}

	/*
	display prints the 2d cityArray array using printf formatting
	*/
	public static void display(int[][] cityArray) {
		for(int i = 0; i < cityArray.length; i++) {
			for(int j = 0; j < cityArray[i].length; j++) {
				System.out.printf("%1$5d ", cityArray[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	/*
	invade adds k amount of invaders onto the map by flipping the value to negative 
	also checks for if the position already has an invader
	*/
	public static void invade(int[][] cityArray, int k) {
		Random rand = new Random();
		int counter = 0;
		while(counter < k) { //iterate from 0 to k 
			int robotX = rand.nextInt(cityArray[0].length-1);
			int robotY = rand.nextInt(cityArray.length-1);

			if(cityArray[robotY][robotX] > 0) { //checks if a robot is on the block
				cityArray[robotY][robotX] = -cityArray[robotY][robotX];
				counter++;
			}
		}
	}

	/*
	update method moves the invader's position one to the right and pushes the 
	invaders at the edge off the map
	*/
	public static void update(int[][] cityArray) {
		for(int i = 0; i < cityArray.length; i++) {
			for(int j = cityArray[i].length - 1; j >= 0; j--) {
				if(cityArray[i][j] < 0) {
					if(j == cityArray[i].length - 1) { //If the robot is at the eastern edge of the subarray
						cityArray[i][j] = -cityArray[i][j]; //Kick the robot off and set the value at index to positive
					}
					else { 
						//move the robot to the next eastern index and reverse the value at the original location
						cityArray[i][j+1] = -cityArray[i][j+1];
						cityArray[i][j] = -cityArray[i][j];
					}
				}
			}
		}
	}

	//Main method
	public static void main(String[] args) {
		int[][] cityArray = buildCity(); //builds city
		System.out.println("Display Original City: ");
		display(cityArray); //prints city
		invade(cityArray, 5); //invade city
		System.out.println("Display Invaded City: ");
		display(cityArray);
		for(int i = 0; i < 5; i++) { //loop the update and display 5 times
			update(cityArray); //updates city
			System.out.println("Display Updated City " + (i+1) + " time: ");
			display(cityArray);
		}
		
	}
}