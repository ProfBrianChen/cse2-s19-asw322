/*
CSE 02 lab 04
Alan Shuo Yang Wang
881596402
*/

//imports math class
import java.lang.Math;

//Create class
public class CardGenerator {

  //Main method
  public static void main(String[] args) {

    //Creates higher and lower bounds from 1 - 52
    int lowerBound = 1;
    int higherBound = 52;

    //uses Math.random to create a random number between 1 and 52
    int myNum = (int) (Math.random() * 51 + 1);

    //declares empty strings
    String suit = "";
    String identity = "";

    //decalre integer temp to get correct integer value within suit
    int temp = 0;

    //if else statements to check what range myNum is within
    if(myNum >= 1 && myNum <= 13) {

      //sets suit to the correct String
      //sets temp to correct integer by subtracting 13 * value
      suit = "Diamonds";
      temp = myNum;
    }
    else if(myNum >= 14 && myNum <= 26) {
      suit = "Clubs";
      temp = myNum - 13 * 1;
    }
    else if(myNum >= 27 && myNum <= 39) {
      suit = "Hearts";
      temp = myNum - 13 * 2;
    }
    else if(myNum >= 40 && myNum <= 52) {
      suit = "Spades";
      temp = myNum - 13 * 3;
    }

    //uses a switch statement to set identity to the correct string
    switch(temp) {
      case 1:
        identity = "Ace";
        break;
      case 2:
        identity = "2";
        break;
      case 3:
        identity = "3";
        break;
      case 4:
        identity = "4";
        break;
      case 5:
        identity = "5";
        break;
      case 6:
        identity = "6";
        break;
      case 7:
        identity = "7";
        break;
      case 8:
        identity = "8";
        break;
      case 9:
        identity = "9";
        break;
      case 10:
        identity = "10";
        break;
      case 11:
        identity = "Jack";
        break;
      case 12:
        identity = "Queen";
        break;
      case 13:
        identity = "King";
        break;

      //this default code should never run
      default:
        identity = null;
        break;
    }

    //prints answer
    System.out.println("You picked the " + identity + " of " + suit);
  }
}
