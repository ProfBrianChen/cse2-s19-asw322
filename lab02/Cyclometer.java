/*
CSE 02 Lab 02
Alan Shuo Yang Wang
881596402
*/

/*
Assignment: My bicycle cyclometer (meant to measure speed, distance, etc)
records two kinds of data, the time elapsed in seconds, and the number of rotations
of the front wheel during that time. For two trips, given time and rotation count, your program should

a. print the number of minutes for each trip
b. print the number of counts for each trips
c. print the distance of each trip in miles
d. print the distance for the two trips combined

*/
public class Cyclometer {
  public static void main(String[] args) {
    //Declaring integers
    int secsTrip1 = 480;
    int secsTrip2 = 3220;
    int countsTrip1 = 1561;
    int countrsTrip2 = 9037;

    //Declaring constant doubles
    double wheelDiameter = 27.0;
    double PI = 3.14159;
    double feetPerMile = 5280;
    double inchesPerFoot = 12;
    double secondsPerMinute = 60;
    double distanceTrip1, distanceTrip2, totalDistance;

    //Prints data to user
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countrsTrip2 + " counts.");

    //distance by rotation for trip 1
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;

    //Converst distanceTrip1 into miles
    distanceTrip1 /= inchesPerFoot * feetPerMile;

    //distance by rotartion for trip 2 and converts into miles
    distanceTrip2 = countrsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;

    //adds distance from both trips to total distance
    totalDistance = distanceTrip1 + distanceTrip2;


    //Prints data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
  }
}
