/*
Alan Shuo Yang Wang
CSE 02 HW 09
881596402
*/

/*
Program description: Create a program that focuses on manipulating arrays and passing arrays into methods.
There are 4 main methods (generate, print, insert, and shorten).
Generate: creates an array with a random length and fills with random integers from 0 - 50
Print: prints the array 
Insert: takes in two arrays as parameters and returns one array with the two arrays joined together at a random index point
Shorten: takes in one array as parameter and one integer returns one array with the value at a target index deleted
NOTE: make sure to print the input and output of each moment when an array is being created or modified
*/

//Import Scanner Arrays and Random Classes
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class ArrayGames {
	public static int[] generate() { //generate method 
		Random rand = new Random();
		int[] arr = new int[rand.nextInt((20 - 10)+1) + 10]; //gets a random length between 20 and 10 inclusive and creates an empty array object
		for(int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt(100); //Fills random integers from 0 to 100
		}
		return arr; //returns single dimensional array arr
	}
	public static void print(int[] arr) { //printing method
		System.out.print("{");
		for(int i = 0; i < arr.length-1; i++) {
			System.out.print(arr[i] + ", ");
		}
		System.out.print(arr[arr.length -1]);
		System.out.println("}\n");
	}
	public static int[] insert(int[] arr1, int[] arr2) { //insert method 
		int[] elem = new int[arr1.length + arr2.length]; //create new empty object with length of two arrays added
		Random rand = new Random();
		int index = rand.nextInt(arr1.length); //gets random index value from 0 to the length or first array inclusive

		int counter = 0;
		for(int i = 0; i < index; i++) { //stores arr1 values from 0 to index exclusive
			elem[counter] = arr1[i];
			counter++;
		}
		for(int i = 0; i < arr2.length; i++) { //stores arr2 values from 0 to arr2.length -1
			elem[counter] = arr2[i];
			counter++;
 		}
 		for(int i = index; i < arr1.length; i++) { //stores arr1 values from index to arr2.length -1
 			elem[counter] = arr1[i];
 			counter++;
 		}
 		return elem; //returns array
	}
	public static int[] shorten(int[] arr, int index) { //shorten method
		int[] resultArr; //creates an uninstantiated int[] array resultArr
		if(index > arr.length - 1) {
			resultArr = arr; //sets arr to resultArr if the index is outside the length 
		}
		else {
			resultArr = new int[arr.length - 1]; //creates array object with 1 less length than arr
			int start = 0; 
			for(int i = 0; i < arr.length; i++) {  
				if(i != index && start == 0) { //copies arr values to resultArr when the index has not been reached 
					resultArr[i] = arr[i];
				}
				else if(i != index && start == 1) { //copies arr values to resultArr with index - 1 when index has been reached
					resultArr[i - 1] = arr[i];
				}
				else { //When the index is equal to the iterator 
					start = 1;
				}
			}
		}
		return resultArr; //return resultArr int array
	}
	public static void main(String[] args) { //main method
		Scanner console = new Scanner(System.in);
		int action = 0;
		int counter = 0;
        while(counter < 1) { //while loop that continuously runs while
            System.out.print("Enter 1 to run insert and 2 to run shorten: ");
            if(console.hasNextInt()) { //checks if the user input is a number
                action = console.nextInt(); //stores user input into action
                if(action == 1 || action == 2) { //checks if action is 1 or 2
                    counter++; //increments counter to exit while loop
                }
                else { //prints error when the number is not 1 or 2
                    System.out.println("Invalid number");
                }
            }
            else { //prints error when user input is wrong data type
                System.out.println("Wrong Data Type");
                String junkword = console.next(); //stores user input into useless String
            }
		  }

		 if(action == 1) {
        	int[] arr = generate(); //calls generate to create random array and store in arr
		 	System.out.print("Input array 1: ");
			print(arr);

			int[] arr2 = generate(); //calls generate to create random array and store in arr2
			System.out.print("Input array 2: ");
			print(arr2);
		
        	int[] resultArr = insert(arr, arr2); //calls insert(int[] arr1, int[] arr2) to combine the array and store in resultArr
		  	System.out.print("Output array: ");
        	print(resultArr);
        }
        else if(action == 2) {
        	
        	int index = -1;
			counter = 0;
	        while(counter < 1) { //while loop that continuously runs while
	            System.out.print("Enter a target integer: ");
	            if(console.hasNextInt()) { //checks if the user input is a number
	                index = console.nextInt(); //stores user input into index
	                if(index >= 0) { //checks if index is a positive integer
	                    counter++; //increments counter to exit while loop
	                }
	                else { //prints error when the number is a negative
	                    System.out.println("Integer cannot be negative");
	                }
	            }
	            else { //prints error when user input is wrong data type
	                System.out.println("Wrong Data Type");
	                String junkword = console.next(); //stores user input into useless String
	            }
	        }
	        int[] arr = generate(); //calls generate to create random array and store in arr
	        System.out.print("Input array: ");
        	print(arr);

        	int[] resultArr = shorten(arr, index); //calls shorten(int[] arr, int index) and stores return array to resultArr 
        	System.out.print("Output array: ");
        	print(resultArr);
        }
	}
}
