/*
* Alan Shuo Yang Wang
* CSE 2 Hw 04
* 881596402
* */

//PokerHandCheck class
public class PokerHandCheck {

    //Main method
    public static void main(String[] args) {

        //Data fields for boolean
        boolean a = false;
        boolean b = false;
        boolean c = false;

        //empty string for suit
        String suit1 = "";
        String suit2 = "";
        String suit3 = "";
        String suit4 = "";
        String suit5 = "";

        //empty string for identity
        String id1 = "";
        String id2 = "";
        String id3 = "";
        String id4 = "";
        String id5 = "";

        //integer for each random card number
        int temp1 = 0;
        int temp2 = 0;
        int temp3 = 0;
        int temp4 = 0;
        int temp5 = 0;

        //creates randNum variable and gets a random number between 1 and 52 using math.random
        int randNum = (int) (Math.random() * 52 + 1);

        //Card 1

        //if number is in Diamond
        if(randNum >= 1 && randNum <= 13) {
            suit1 = "Diamonds";
            temp1 = randNum;
        }

        //if number is in Clubs
        else if(randNum >= 14 && randNum <= 26) {
            suit1 = "Clubs";

            //temp is set to minus 13
            temp1 = randNum - 13 * 1;
        }

        //if number is in Hearts
        else if(randNum >= 27 && randNum <= 39) {
            suit1 = "Hearts";

            //temp is set to minus 26
            temp1 = randNum - 13 * 2;
        }

        //if number is in Spades
        else if(randNum >= 40 && randNum <= 52) {
            suit1 = "Spades";

            //temp is set to minus 39
            temp1= randNum - 13 * 3;
        }

        //switch statement for the card number and sets the identity of card to the appropriate number or word
        switch(temp1) {
            case 1:
                id1 = "Ace";
                break;
            case 2:
                id1 = "2";
                break;
            case 3:
                id1 = "3";
                break;
            case 4:
                id1 = "4";
                break;
            case 5:
                id1 = "5";
                break;
            case 6:
                id1 = "6";
                break;
            case 7:
                id1 = "7";
                break;
            case 8:
                id1 = "8";
                break;
            case 9:
                id1 = "9";
                break;
            case 10:
                id1 = "10";
                break;
            case 11:
                id1 = "Jack";
                break;
            case 12:
                id1 = "Queen";
                break;
            case 13:
                id1 = "King";
                break;
            default:
                id1 = null;
                break;
        }

        //Card 2
        randNum = (int) (Math.random() * 52 + 1);

        if(randNum >= 1 && randNum <= 13) {
            suit2 = "Diamonds";
            temp2 = randNum;
        }
        else if(randNum >= 14 && randNum <= 26) {
            suit2 = "Clubs";
            temp2 = randNum - 13 * 1;
        }
        else if(randNum >= 27 && randNum <= 39) {
            suit2 = "Hearts";
            temp2 = randNum - 13 * 2;
        }
        else if(randNum >= 40 && randNum <= 52) {
            suit2 = "Spades";
            temp2 = randNum - 13 * 3;
        }
        switch(temp2) {
            case 1:
                id2 = "Ace";
                break;
            case 2:
                id2 = "2";
                break;
            case 3:
                id2 = "3";
                break;
            case 4:
                id2 = "4";
                break;
            case 5:
                id2 = "5";
                break;
            case 6:
                id2 = "6";
                break;
            case 7:
                id2 = "7";
                break;
            case 8:
                id2 = "8";
                break;
            case 9:
                id2 = "9";
                break;
            case 10:
                id2 = "10";
                break;
            case 11:
                id2 = "Jack";
                break;
            case 12:
                id2 = "Queen";
                break;
            case 13:
                id2 = "King";
                break;
            default:
                id2 = null;
                break;
        }


        randNum = (int) (Math.random() * 52 + 1);
        if(randNum >= 1 && randNum <= 13) {
            suit3 = "Diamonds";
            temp3 = randNum;
        }
        else if(randNum >= 14 && randNum <= 26) {
            suit3 = "Clubs";
            temp3 = randNum - 13 * 1;
        }
        else if(randNum >= 27 && randNum <= 39) {
            suit3 = "Hearts";
            temp3 = randNum - 13 * 2;
        }
        else if(randNum >= 40 && randNum <= 52) {
            suit3 = "Spades";
            temp3 = randNum - 13 * 3;
        }
        switch(temp3) {
            case 1:
                id3 = "Ace";
                break;
            case 2:
                id3 = "2";
                break;
            case 3:
                id3 = "3";
                break;
            case 4:
                id3 = "4";
                break;
            case 5:
                id3 = "5";
                break;
            case 6:
                id3 = "6";
                break;
            case 7:
                id3 = "7";
                break;
            case 8:
                id3 = "8";
                break;
            case 9:
                id3 = "9";
                break;
            case 10:
                id3 = "10";
                break;
            case 11:
                id3 = "Jack";
                break;
            case 12:
                id3 = "Queen";
                break;
            case 13:
                id3 = "King";
                break;
            default:
                id3 = null;
                break;
        }

        //Card 4
        randNum = (int) (Math.random() * 52 + 1);
        if(randNum >= 1 && randNum <= 13) {
            suit4 = "Diamonds";
            temp4 = randNum;
        }
        else if(randNum >= 14 && randNum <= 26) {
            suit4 = "Clubs";
            temp4 = randNum - 13 * 1;
        }
        else if(randNum >= 27 && randNum <= 39) {
            suit4 = "Hearts";
            temp4 = randNum - 13 * 2;
        }
        else if(randNum >= 40 && randNum <= 52) {
            suit4 = "Spades";
            temp4 = randNum - 13 * 3;
        }
        switch(temp4) {
            case 1:
                id4 = "Ace";
                break;
            case 2:
                id4 = "2";
                break;
            case 3:
                id4 = "3";
                break;
            case 4:
                id4 = "4";
                break;
            case 5:
                id4 = "5";
                break;
            case 6:
                id4 = "6";
                break;
            case 7:
                id4 = "7";
                break;
            case 8:
                id4 = "8";
                break;
            case 9:
                id4 = "9";
                break;
            case 10:
                id4 = "10";
                break;
            case 11:
                id4 = "Jack";
                break;
            case 12:
                id4 = "Queen";
                break;
            case 13:
                id4 = "King";
                break;
            default:
                id4 = null;
                break;
        }

        //Card 5
        randNum = (int) (Math.random() * 52 + 1);
        if(randNum >= 1 && randNum <= 13) {
            suit5 = "Diamonds";
            temp5 = randNum;
        }
        else if(randNum >= 14 && randNum <= 26) {
            suit5 = "Clubs";
            temp5 = randNum - 13 * 1;
        }
        else if(randNum >= 27 && randNum <= 39) {
            suit5 = "Hearts";
            temp5 = randNum - 13 * 2;
        }
        else if(randNum >= 40 && randNum <= 52) {
            suit5 = "Spades";
            temp5 = randNum - 13 * 3;
        }
        switch(temp5) {
            case 1:
                id5 = "Ace";
                break;
            case 2:
                id5 = "2";
                break;
            case 3:
                id5 = "3";
                break;
            case 4:
                id5 = "4";
                break;
            case 5:
                id5 = "5";
                break;
            case 6:
                id5 = "6";
                break;
            case 7:
                id5 = "7";
                break;
            case 8:
                id5 = "8";
                break;
            case 9:
                id5 = "9";
                break;
            case 10:
                id5 = "10";
                break;
            case 11:
                id5 = "Jack";
                break;
            case 12:
                id5 = "Queen";
                break;
            case 13:
                id5 = "King";
                break;
            default:
                id5 = null;
                break;
        }

        //Printing random numbers
        System.out.println("Your random cards were: ");

        System.out.println("  the " + id1 + " of " + suit1);
        System.out.println("  the " + id2 + " of " + suit2);
        System.out.println("  the " + id3 + " of " + suit3);
        System.out.println("  the " + id4 + " of " + suit4);
        System.out.println("  the " + id5 + " of " + suit5);


        //First checks if the cards have three of a kind by running all possible tests
        if(temp1 == temp2 && temp1 == temp3) {
            c = true;
        }
        else if(temp1 == temp2 && temp1 == temp4) {
            c = true;
        }
        else if(temp1 == temp2 && temp1 == temp5) {
            c = true;
        }
        else if(temp1 == temp3 && temp1 == temp4) {
            c = true;
        }
        else if(temp1 == temp3 && temp1 == temp5) {
            c = true;
        }
        else if(temp1 == temp4 && temp1 == temp5) {
            c = true;
        }
        else if(temp2 == temp3 && temp2 == temp4) {
            c = true;
        }
        else if(temp2 == temp3 && temp2 == temp5) {
            c = true;
        }
        else if(temp2 == temp4 && temp2 == temp5) {
            c = true;
        }
        else if(temp3 == temp4 && temp3 == temp5) {
            c = true;
        }

        //Then checks if there are 2 pairs by running all possible tests
        if(temp1 == temp2 && temp3 == temp4) {
            b = true;
        }
        else if(temp1 == temp4 && temp2 == temp3) {
            b = true;
        }
        else if(temp1 == temp3 && temp2 == temp4) {
            b = true;
        }
        else if(temp1 == temp5 && temp2 == temp3) {
            b = true;
        }
        else if(temp1 == temp5 && temp2 == temp4) {
            b = true;
        }
        else if(temp1 == temp5 && temp3 == temp4) {
            b = true;
        }
        else if(temp2 == temp5 && temp3 == temp4) {
            b = true;
        }
        else if(temp2 == temp4 && temp3 == temp5) {
            b = true;
        }
        else if(temp2 == temp3 && temp4 == temp5) {
            b = true;
        }

        //Finally checks if there is 1 pair by running all possible tests
        if(temp1 == temp2) {
            a = true;
        }
        else if(temp1 == temp3) {
            a = true;
        }
        else if(temp1 == temp4) {
            a = true;
        }
        else if(temp1 == temp5) {
            a = true;
        }
        else if(temp2 == temp3) {
            a = true;
        }
        else if(temp2 == temp4) {
            a = true;
        }
        else if(temp2 == temp5) {
            a = true;
        }
        else if(temp3 == temp4) {
            a = true;
        }
        else if(temp3 == temp5) {
            a = true;
        }
        else if(temp4 == temp5) {
            a = true;
        }

        //if c is true you have three of a kind
        if(c == true) {
            System.out.println("\nYou have three of a kind!");
        }

        //if c is not true
        else if(c == false) {

            //if b is true print you have two pair
            if(b == true) {
                System.out.println("\nYou have a two pair!");
            }

            //if b is not true
            else if(b == false) {

                //if a is true print you have a pair
                if(a == true) {
                    System.out.println("\nYou have a pair!");
                }

                //if a is not true then you have a high card
                else if(a == false) {
                    System.out.println("\nYou have a high card hand!");
                }
            }
        }
    }
}