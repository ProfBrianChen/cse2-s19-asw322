/*
 * Alan Shuo Yang Wang
 * CSE 02 Lab 06
 * 881596402
 * */

import java.util.Scanner; //import scanner object
public class PatternB {
    public static void main(String[] args) { //main method
        Scanner console = new Scanner(System.in); //Create scanner object
        int a = 0; //pyramid size
        int counter = 0;

        while(counter < 1) { //while loop that continuously runs while
            System.out.print("Enter an integer between 1 - 10: ");
            if(console.hasNextInt()) { //checks if the user input is a number
                a = console.nextInt(); //stores user input into myInt
                if(a > 0 && a <10) { //checks if myInt is a positive integer
                    counter++; //increments counter to exit while loop
                }
                else { //prints error when the number is a negative
                    System.out.println("Cannot accept negative number");
                }
            }
            else { //prints error when user input is wrong data type
                System.out.println("Wrong Data Type");
                String junkword = console.next(); //stores user input into useless String
            }
        }

        //Prints pyramid
        for(int i = a; i >= 1; i--) { //iterates from 6 to 1 to create 6 rows
            for(int j = 1; j <= i; j++) { //iterates from 1 to i to create slanted number
                System.out.print(j); //prints num
            }
            System.out.println(); //prints a new line after row
        }
    }
}